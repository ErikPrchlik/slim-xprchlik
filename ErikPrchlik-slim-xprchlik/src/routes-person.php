<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    $q = $request->getQueryParam('q');

    try {
        if (empty($q)) {
            $stmt = $this->db->prepare('SELECT person.*, location.*, COALESCE(pocet_k,0) AS pocet_k, COALESCE(pocet_s,0) AS pocet_s, COALESCE(pocet_v,0) AS pocet_v
                FROM person
                LEFT JOIN location USING (id_location)
                LEFT JOIN (
                  SELECT id_person, COUNT(*) AS pocet_k
                  FROM contact
                  GROUP BY id_person
                ) AS pocty_kontaktu USING (id_person)
                LEFT JOIN (
                  SELECT id_person, COUNT(*) AS pocet_s
                  FROM person_meeting
                  GROUP BY id_person
                ) AS pocty_schuzek USING (id_person)
                LEFT JOIN (
                  select id_person, COUNT(*) AS pocet_v from person
                  join relation on (id_person=id_person1) or (id_person=id_person2)
                  GROUP BY id_person
                ) AS pocty_vztahov USING (id_person)
                ORDER BY last_name');
        } else {
            $stmt = $this->db->prepare('SELECT person.*, location.*, COALESCE(pocet_k,0) AS pocet_k, COALESCE(pocet_s,0) AS pocet_s, COALESCE(pocet_v,0) AS pocet_v
                FROM person
                LEFT JOIN location USING (id_location)
                LEFT JOIN (
                  SELECT id_person, COUNT(*) AS pocet_k
                  FROM contact
                  GROUP BY id_person
                ) AS pocty_kontaktu USING (id_person)
                LEFT JOIN (
                  SELECT id_person, COUNT(*) AS pocet_s
                  FROM person_meeting
                  GROUP BY id_person
                ) AS pocty_schuzek USING (id_person)
                LEFT JOIN (
                  select id_person, COUNT(*) AS pocet_v from person
                  join relation on (id_person=id_person1) or (id_person=id_person2)
                  GROUP BY id_person
                ) AS pocty_vztahov USING (id_person)
                WHERE last_name ILIKE :q OR
                first_name ILIKE :q OR 
                nickname ILIKE :q
                ORDER BY last_name');
            $stmt->bindValue(':q', $q . '%');
        }
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['people'] = $stmt->fetchAll();
    $tplVars['q'] = $q;
    return $this->view->render($response, 'persons.latte', $tplVars
    );
})->setName('persons');

$app->get('/add-person', function (Request $request, Response $response, $args) {
    $tplVars['form'] = ['ln' => '', 'fn' => '', 'nn' => '', 'v' => 180, 'g' => '', 'bd' => '', 'ci' => '', 'st' => '',
        'cd' => '', 'psc' => '', 'fb' => '', 'em' => '', 'tel' => ''];
    return $this->view->render($response, 'add-person.latte', $tplVars);
})->setName('add-person');

$app->post('/add-person', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();  //$_POST
    if (!empty($data['fn']) && !empty($data['nn']) && !empty($data['ln'])) {
        try {
            $this->db->beginTransaction();
            $idLocation = null;
            if (!empty($data['ci'])) {
                $stmt = $this->db->prepare('INSERT INTO location
                                          (city, street_name, street_number, zip)
                                           VALUES 
                                          (:ci, :st, :cd, :psc)');
                $stmt->bindValue(':ci', $data['ci']);
                $stmt->bindValue(':st', $data['st']);
                $stmt->bindValue(':cd', empty($data['cd']) ? null : $data['cd']);
                $stmt->bindValue(':psc', $data['psc']);
                $stmt->execute();
                $idLocation = $this->db->lastInsertId('location_id_location_seq');
            }
            $idPerson = null;
            $stmt = $this->db->prepare('INSERT INTO person
                              (id_location, birth_day, height, gender, last_name, first_name, nickname)
                              VALUES
                              (:idl, :bd, :v, :g, :ln, :fn, :nn)');
            $g = empty($data['g']) ? null : $data['g'];
            $v = empty($data['v']) ? null : $data['v'];
            $bd = empty($data['bd']) ? null : $data['bd'];
            $stmt->bindValue(':idl', $idLocation);
            $stmt->bindValue(':g', $g);
            $stmt->bindValue(':v', $v);
            $stmt->bindValue(':bd', $bd);
            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->execute();
            $idPerson = $this->db->lastInsertId('person_id_person_seq');
            if (!empty($data['fb'])) {
                $stmt = $this->db->prepare('INSERT INTO contact
                                          (id_person, id_contact_type, contact)
                                           VALUES 
                                          (:idp, 1, :fb)');
                $stmt->bindValue(':idp', $idPerson);
                $stmt->bindValue(':fb', $data['fb']);
                $stmt->execute();
            }
            if (!empty($data['em'])) {
                $stmt = $this->db->prepare('INSERT INTO contact
                                          (id_person, id_contact_type, contact)
                                           VALUES 
                                          (:idp, 4, :em)');
                $stmt->bindValue(':idp', $idPerson);
                $stmt->bindValue(':em', $data['em']);
                $stmt->execute();
            }
            if (!empty($data['tel'])) {
                $stmt = $this->db->prepare('INSERT INTO contact
                                          (id_person, id_contact_type, contact)
                                           VALUES 
                                          (:idp, 3, :tel)');
                $stmt->bindValue(':idp', $idPerson);
                $stmt->bindValue(':tel', $data['tel']);
                $stmt->execute();
            }
            $this->db->commit();
        } catch (Exception $ex) {
            $this->db->rollback();
            if ($ex->getCode() == 23505) {
                $tplVars['error'] = 'Tato osoba uz existuje.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'add-person.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
        }
        return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/detail-person?id=$idPerson");
    } else {
        $tplVars['error'] = 'Nejsou vyplneny vsechny udaje.';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'add-person.latte', $tplVars);
    }
});

$app->get('/edit-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('SELECT * FROM person WHERE id_person=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $person = $stmt->fetch();
    $tplVars['form'] = [
        'ln' => $person['last_name'],
        'fn' => $person['first_name'],
        'nn' => $person['nickname'],
        'h' => $person['height'],
        'g' => $person['gender'],
        'bd' => $person['birth_day']];

    return $this->view->render($response, 'edit-person.latte', $tplVars);
})->setName('edit-person');

$app->post('/edit-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();
    if (!empty($data['fn']) && !empty($data['ln']) && !empty($data['nn'])) {
        try {
            $stmt = $this->db->prepare('UPDATE person SET
                first_name = :fn, last_name = :ln, nickname = :nn,
                gender = :g, height = :h, birth_day = :bd
              WHERE id_person = :id');
            $h = empty($data['h']) ? null : $data['h'];
            $g = empty($data['g']) ? null : $data['g'];
            $bd = empty($data['bd']) ? null : $data['bd'];
            $stmt->bindValue(':id', $id);
            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->bindValue(':g', $g);
            $stmt->bindValue(':h', $h);
            $stmt->bindValue(':bd', $bd);
            $stmt->execute();
        } catch (Exception $e) {
            if ($e->getCode() == 23505) {
                $tplVars['error'] = 'Tato osoba uz existuje.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'edit-person.latte', $tplVars);
            } else {
                $this->logger->error($e->getMessage());
                die($e->getMessage());
            }
        }
        return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/detail-person?id=$id");
    } else {
        $tplVars['error'] = 'Vyplnte povinne udaje.';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'edit-person.latte', $tplVars);
    }
});

$app->post('/delete-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try {
        $stmt = $this->db->prepare('DELETE FROM person WHERE id_person=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('persons'));
})->setName('delete-person');

$app->get('/detail-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('select * from person 
                                    left join (select count(*) as pocet_k,id_person from contact group by (id_person)) 
                                    as con using (id_person)
                                    left join (select count(*) as pocet_s,id_person from person_meeting group by (id_person))
                                    as meet using (id_person)
                                    left join location using(id_location)
                                    where id_person=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $person = $stmt->fetch();
    $tplVars['form'] = [
        'id' => $person['id_person'],
        'ln' => $person['last_name'],
        'fn' => $person['first_name'],
        'nn' => $person['nickname'],
        'k' => empty($person['pocet_k']) ? 0 : $person['pocet_k'],
        's' => empty($person['pocet_s']) ? 0 : $person['pocet_s'],
        'c' => $person['city'],
        'st' => $person['street_name'],
        'stn' => $person['street_number'],
        'co' => $person['country']];
    try {
        $stmt1 = $this->db->prepare('SELECT *
                                    FROM contact 
                                    LEFT JOIN(
                                    SELECT id_contact_type, name
                                    FROM contact_type
                                    GROUP BY id_contact_type
                                    ) AS con USING (id_contact_type)
                                    left join 
                                    (select id_person, first_name from person
                                    ) as per using (id_person)
                                    WHERE id_person=:id
                                    ORDER BY name');
        $stmt1->bindValue(':id', $id);
        $stmt1->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['contacts'] = $stmt1->fetchAll();
    try {
        $stmt2 = $this->db->prepare('select relation.id_relation, id_person, first_name, last_name, 
                                    relation.id_relation_type, rel.name, id_person1, id_person2 from person
                                    join relation on (person.id_person = relation.id_person1) 
                                    or (person.id_person = relation.id_person2)
                                    LEFT JOIN(
                                    SELECT id_relation_type, name
                                    FROM relation_type
                                    GROUP BY id_relation_type
                                    ) AS rel USING (id_relation_type)
                                    where ((id_person1 = :id) or (id_person2 = :id)) and (id_person <> :id)
                                    order by relation.id_relation_type, first_name');
        $stmt2->bindValue(':id', $id);
        $stmt2->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['relations'] = $stmt2->fetchAll();
    return $this->view->render($response, 'detail-person.latte', $tplVars);
})->setName('detail-person');
