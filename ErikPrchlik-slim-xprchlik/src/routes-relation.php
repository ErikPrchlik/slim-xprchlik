<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/add-relation', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('SELECT DISTINCT(first_name, last_name) as person, id_person FROM person');
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['persons'] = $stmt->fetchAll();
    try {
        $stmt = $this->db->prepare('select name from relation_type');
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['relation_type'] = $stmt->fetchAll();
    return $this->view->render($response, 'add-relation.latte', $tplVars);
})->setName('add-relation');

$app->post('/add-relation', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();
    $idp2 = $data['person'];
    if (!($data['name'] == "") or !($idp2 == null)) {
        try {
            $stmt = $this->db->prepare('select id_relation_type from relation_type where name = :nm');
            $stmt->bindValue(':nm', $data['name']);
            $stmt->execute();
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die($ex->getMessage());
        }
        $relation_type = $stmt->fetchAll();
        $relation_type = $relation_type[0];
        try {
            $stmt = $this->db->prepare('INSERT INTO relation
                                          (id_person1, id_person2, description, id_relation_type)
                                           VALUES
                                          (:idp1, :idp2, \'\', :idt)');
            $stmt->bindValue(':idp1', $id);
            $stmt->bindValue(':idp2', $idp2);
            $stmt->bindValue(':idt', $relation_type['id_relation_type']);
            $stmt->execute();
        } catch (Exception $ex) {
            if ($ex->getCode() == 23505) {
                $tplVars['error'] = 'Tento vzťah už existuje.';
                $tplVars['form'] = $data;
                $tplVars['relation_type'] = $data;
                return $this->view->render($response, 'add-relation.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
        }
    } else {
        $tplVars['error'] = 'Zle zadaný vzťah.';
        $tplVars['relation_type'] = $data;
        return $this->view->render($response, 'add-relation.latte', $tplVars);
    }
    return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/detail-person?id=$id");
});

$app->post('/delete-relation', function (Request $request, Response $response, $args) {
    $idr = $request->getQueryParam('idr');
    $data = $request->getParsedBody();
    $idp=$data['idp'];
    try {
        $stmt = $this->db->prepare('DELETE FROM relation WHERE id_relation=:idr');
        $stmt->bindValue(':idr', $idr);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/detail-person?id=$idp");
})->setName('delete-relation');

$app->get('/edit-relation', function (Request $request, Response $response, $args) {
    $idr = $request->getQueryParam('idr');
    $idp = $request->getQueryParam('idp');
    try {
        $stmt = $this->db->prepare('select name from relation_type');
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['relation_type'] = $stmt->fetchAll();
    try {
        $stmt = $this->db->prepare('SELECT * FROM relation
                                    join (select * from person where id_person<>:idp) 
                                    as p on (id_person=id_person1) or (id_person=id_person2)
                                    WHERE id_relation=:idr  ');
        $stmt->bindValue(':idr', $idr);
        $stmt->bindValue(':idp', $idp);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $relation = $stmt->fetch();
    $tplVars['form'] = [
        'idp1' => $relation['id_person1'],
        'idp2' => $relation['id_person2'],
        'des' => $relation['description'],
        'fn' => $relation['first_name'],
        'ln' => $relation['last_name'],
        'nn' => $relation['nickname']];
    $tplVars['idr'] =$idr;
    $tplVars['idp'] =$idp;
    return $this->view->render($response, 'edit-relation.latte', $tplVars);
})->setName('edit-relation');

$app->post('/edit-relation', function (Request $request, Response $response, $args) {
    $idr = $request->getQueryParam('idr');
    $data = $request->getParsedBody();
    $idp = $data['idp'];
    try {
        $stmt1 = $this->db->prepare('select id_person from person
                                    where (first_name = :fn) and (last_name = :ln) and (nickname = :nn)');
        $stmt1->bindValue(':fn', $data['fn']);
        $stmt1->bindValue(':ln', $data['ln']);
        $stmt1->bindValue(':nn', $data['nn']);
        $stmt1->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $idp2 = $stmt1->fetch();
    $idp2 = $idp2['id_person'];
    if (!($data['name'] == "") or !($idp2 == null)) {
        try {
            try {
                $stmt = $this->db->prepare('select id_relation_type from relation_type where name = :nm');
                $stmt->bindValue(':nm', $data['name']);
                $stmt->execute();
            } catch (Exception $ex) {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
            $relation_type = $stmt->fetchAll();
            $relation_type = $relation_type[0];
            $stmt = $this->db->prepare('UPDATE relation SET
                              id_person1 = :idp, id_person2 = :idp2, id_relation_type = :idt
                              WHERE id_relation = :idr');
            $stmt->bindValue(':idp', $idp);
            $stmt->bindValue(':idp2', $idp2);
            $stmt->bindValue(':idt', $relation_type['id_relation_type']);
            $stmt->bindValue(':idr', $idr);
            $stmt->execute();
        } catch (Exception $ex) {
            if ($ex->getCode() == 23505) {
                $tplVars['error'] = 'Tento vyťah už existuje.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'edit-relation.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
        }
        return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/detail-person?id=$idp");
    } else {
        $tplVars['error'] = 'Nie sú vyplnené všetky údaje.';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'edit-relation.latte', $tplVars);
    }
});