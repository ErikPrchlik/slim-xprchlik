<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->get('/meetings', function (Request $request, Response $response, $args) {
    try {
        $stmt = $this->db->prepare('select meeting.*, location.*, COALESCE(poc_u ,0) AS poc_u from meeting
                                    left join (select id_meeting, count(id_person) as poc_u 
                                          from person_meeting 
                                          group by id_meeting
                                    ) as pm using (id_meeting)
                                    left join location using (id_location)
                                    order by start, duration');
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['meetings'] = $stmt->fetchAll();
    return $this->view->render($response, 'meetings.latte', $tplVars);
})->setName('meetings');

$app->get('/meetings-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('select * from person
                                    where id_person=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $person = $stmt->fetch();
    $tplVars['person'] = $person['first_name'];
    try {
        $stmt = $this->db->prepare('select * from person_meeting
                                    join (select * from meeting) as me using (id_meeting)
                                    join (select * from location) as lo using (id_location)
                                    join (select id_meeting, count(id_person) as poc_u
                                    from person_meeting group by id_meeting) as uc using (id_meeting)
                                    where id_person=:id
                                    order by start, duration');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['meetings'] = $stmt->fetchAll();
    return $this->view->render($response, 'meetings-person.latte', $tplVars);
})->setName('meetings-person');

$app->get('/uc-meeting', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('select id_meeting, start from meeting
                                    where id_meeting=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $meeting = $stmt->fetch();
    $tplVars['meeting'] = $meeting['start'];
    try {
        $stmt = $this->db->prepare('select * from person_meeting
                                    join (select id_person, first_name, last_name from person) 
                                    as uc using (id_person)
                                    where id_meeting=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['uc'] = $stmt->fetchAll();
    return $this->view->render($response, 'uc-meeting.latte', $tplVars);
})->setName('uc-meeting');

$app->get('/add-meeting', function (Request $request, Response $response, $args) {
    $tplVars['form'] = ['start' => '', 'desc' => '', 'dur' => '', 'city' => '', 'stna' => '', 'stnu' => ''];
    return $this->view->render($response, 'add-meeting.latte', $tplVars);
})->setName('add-meeting');

$app->post('/add-meeting', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (!(empty($data['city'])) and (!empty($data['stna']) ) and (!empty($data['stnu']) )) {
        try {
            $stmt1 = $this->db->prepare('select id_location from location
                                         where (city = :city) and (street_name = :stna) and (street_number = :stnu)');
            $stmt1->bindValue(':city', $data['city']);
            $stmt1->bindValue(':stna', $data['stna']);
            $stmt1->bindValue(':stnu', $data['stnu']);
            $stmt1->execute();
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die($ex->getMessage());
        }
        $idl=$stmt1->fetch();
    } else if (!empty($data['city'])) {
        try {
            $stmt1 = $this->db->prepare('select id_location from location
                                         where (city = :city) and (street_name is null) and (street_number is null)');
            $stmt1->bindValue(':city', $data['city']);
            $stmt1->execute();
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die($ex->getMessage());
        }
        $idl=$stmt1->fetch();
    } else {
        $tplVars['error'] = 'Nesprávne zadané miesto konania.';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'add-meeting.latte', $tplVars);
    }
    $idl = empty($idl) ? null : $idl['id_location'];
    if (($idl<>null) and ($data['dur']='')) {
        try {
            $stmt = $this->db->prepare('INSERT INTO meeting
                                          (start, description, duration, id_location)
                                           VALUES
                                          (:start, :desc, :dur, :idl)');
            $stmt->bindValue(':start', date("Y-m-d H:i:s"));
            $stmt->bindValue(':desc', $data['desc']);
            $stmt->bindValue(':dur', date("Y-m-d H:i:s"));
            $stmt->bindValue(':idl', $idl);
            $stmt->execute();
        } catch (Exception $ex) {
            if ($ex->getCode() == 23505) {
                $tplVars['error'] = 'Toto stretnutie už existuje.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'add-meeting.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('meetings'));
});

$app->get('/meeting-add-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('SELECT DISTINCT(first_name, last_name) as person, id_person FROM person');
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['persons'] = $stmt->fetchAll();
    try {
        $stmt = $this->db->prepare('SELECT * FROM meeting where id_meeting=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['meeting'] = $stmt->fetch();
    return $this->view->render($response, 'meeting-add-person.latte', $tplVars);
})->setName('meeting-add-person');

$app->post('/meeting-add-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('SELECT * FROM meeting where id_meeting=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['meeting'] = $stmt->fetch();
    try {
        $stmt = $this->db->prepare('SELECT DISTINCT(first_name, last_name) as person, id_person FROM person');
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['persons'] = $stmt->fetchAll();
    if ($data['person']!=''){
        try {
            $stmt = $this->db->prepare('INSERT INTO person_meeting
                                          (id_person, id_meeting)
                                           VALUES
                                          (:idp, :idm)');
            $stmt->bindValue(':idp', $data['person']);
            $stmt->bindValue(':idm', $id);
            $stmt->execute();
        } catch (Exception $ex) {
            if ($ex->getCode() == 23505) {
                $tplVars['error'] = 'Toto stretnutie už obsahuje konkretnu osobu.';
                return $this->view->render($response, 'meeting-add-person.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
        }
    } else {
        $tplVars['error'] = 'Nebola zvolená osoba.';
        return $this->view->render($response, 'meeting-add-person.latte', $tplVars);
    }
    return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/uc-meeting?id=$id");
});

$app->post('/meeting-delete-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare('DELETE FROM person_meeting WHERE id_person=:id');
        $stmt->bindValue(':id', $data['idp']);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/uc-meeting?id=$id");
})->setName('meeting-delete-person');

$app->get('/person-add-meeting', function (Request $request, Response $response, $args) {
    $idp = $request->getQueryParam('id');
    $tplVars['id'] = $idp;
    try {
        $stmt = $this->db->prepare('SELECT (start, city, street_name, street_number, description) as data, id_meeting FROM meeting JOIN location 
                                    using(id_location)
                                    order by start');

        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['meetings'] = $stmt->fetchAll();
    try {
        $stmt = $this->db->prepare('SELECT * FROM person where id_person=:idp');
        $stmt->bindValue(':idp', $idp);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['person'] = $stmt->fetch();
    return $this->view->render($response, 'person-add-meeting.latte', $tplVars);
})->setName('person-add-meeting');

$app->post('/person-add-meeting', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('SELECT * FROM person where id_person=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['person'] = $stmt->fetch();
    try {
        $stmt = $this->db->prepare('SELECT (start, city, street_name, street_number, description) as data, id_meeting FROM meeting 
                                    JOIN location using(id_location)
                                    order by start');
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['meetings'] = $stmt->fetchAll();
    if ($data['meeting']!=''){
        try {
            $stmt = $this->db->prepare('INSERT INTO person_meeting
                                          (id_person, id_meeting)
                                           VALUES
                                          (:idp, :idm)');
            $stmt->bindValue(':idp', $id);
            $stmt->bindValue(':idm', $data['meeting']);
            $stmt->execute();
        } catch (Exception $ex) {
            if ($ex->getCode() == 23505) {
                $tplVars['error'] = 'Osoba už má toto stretnutie.';
                return $this->view->render($response, 'person-add-meeting.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
        }
    } else {
        $tplVars['error'] = 'Nebolo zvolené stretnutie.';
        return $this->view->render($response, 'person-add-meeting.latte', $tplVars);
    }
    return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/meetings-person?id=$id");
});

$app->post('/person-delete-meeting', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare('DELETE FROM person_meeting WHERE id_meeting=:id');
        $stmt->bindValue(':id', $data['idm']);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/meetings-person?id=$id");
})->setName('person-delete-meeting');