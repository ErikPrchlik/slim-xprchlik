<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->get('/', function (Request $request, Response $response){
    return $response->withHeader('Location', $this->router->pathFor('persons'));
});

$app->group('/auth', function () use ($app){
    include('routes-person.php');
    include('routes-contact.php');
    include('routes-relation.php');
    include('routes-meeting.php');
    $app->get('/logout', function (Request $request, Response $response){
        session_destroy();
        return $response->withHeader('Location', $this->router->pathFor('login'));
    })->setName('logout');
})->add(function (Request $request, Response $response, $next){
    if (!empty($_SESSION['user'])){
        return$next($request, $response);
    } else{
        return $response->withHeader('Location', $this->router->pathFor('login'));
    }
});

include ('routes-login.php');















//$app->get('/', function (Request $request, Response $response, $args) {
//    // Render index view
//    return $this->view->render($response, 'index.latte');
//})->setName('index');
//
//$app->post('/test', function (Request $request, Response $response, $args) {
//    //read POST data
//    $input = $request->getParsedBody();
//
//    //log
//    $this->logger->info('Your name: ' . $input['person']);
//
//    return $response->withHeader('Location', $this->router->pathFor('index'));
//})->setName('redir');
