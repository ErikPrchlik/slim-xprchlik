<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/login', function (Request $request, Response $response, $args) {

    return $this->view->render($response,'login.latte');
})->setName('login');

$app->post('/login', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if((!empty($data['login'])) && (!empty($data['pass']))) {
        try {
            $stmt = $this->db->prepare('SELECT * FROM account WHERE login = :l');
            $stmt->bindValue(':l', $data['login']);
            $stmt->execute();
            $user = $stmt->fetch();
            if (!empty($user)){
                if (password_verify($data['pass'], $user['password'])){
                    $_SESSION['user']=$user;
                    return$response->withHeader('Location', $this->router->pathFor('persons'));
                }
            }
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die($ex->getMessage());
        }
    }
    $tplVars['error']="Chyba prihlasenia";
    return $this->view->render($response, 'login.latte', $tplVars);
});

$app->get('/registration', function (Request $request, Response $response, $args) {
    $tplVars['form'] = ['login' => '', 'password' => ''];
    return $this->view->render($response,'registration.latte', $tplVars);
})->setName('registration');

$app->post('/registration', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
    if(!empty($data['login']) && !empty($data['password'])) {
        try {
            $stmt=$this->db->prepare('INSERT INTO account
                                      (login, password)
                                       VALUES 
                                      (:lo, :pw)');
            $stmt->bindValue(':lo', $data['login']);
            $stmt->bindValue(':pw', $data['password']);
            $stmt->execute();
        } catch (Exception $ex) {
            if($ex->getCode() == 23505) {
                $tplVars['error'] = 'Uživateľ už existuje.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'registration.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
        }
        return $response->withHeader('Location', $this->router->pathFor('persons'));
    } else {
        $tplVars['error'] = 'Nejsou vyplneny vsechny udaje.';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'registration.latte', $tplVars);
    }
});