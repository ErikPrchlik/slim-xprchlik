<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/edit-contact', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('SELECT * FROM contact 
                                    join contact_type using(id_contact_type)
                                    WHERE id_contact=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $contact = $stmt->fetch();
    $tplVars['form'] = [
        'idc' => $contact['id_contact'],
        'idp' => $contact['id_person'],
        'idt' => $contact['id_contact_type'],
        'name' => $contact['name'],
        'co' => $contact['contact']];

    return $this->view->render($response, 'edit-contact.latte', $tplVars);
})->setName('edit-contact');

$app->post('/edit-contact', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();

    if (!empty($data['co'])) {
        try {
            $stmt = $this->db->prepare('UPDATE contact SET
                              contact = :co
                              WHERE id_contact = :id');

            $stmt->bindValue(':co', $data['co']);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
        } catch (Exception $ex) {
            if ($ex->getCode() == 23505) {
                $tplVars['error'] = 'Tento kontakt uz existuje.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'edit-contact.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }
        }
        try {
            $stmt1 = $this->db->prepare('select id_person from contact
                                    WHERE id_contact = :id');
            $stmt1->bindValue(':id', $id);
            $stmt1->execute();
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die($ex->getMessage());
        }
        $idp = $stmt1->fetch();
        $idp = $idp['id_person'];
        return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/detail-person?id=$idp");
    } else {
        $tplVars['error'] = 'Nejsou vyplneny vsechny udaje.';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'edit-contact.latte', $tplVars);
    }
});


$app->post('/delete-contact', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try {
        $stmt1 = $this->db->prepare('select id_person from contact
                                    WHERE id_contact = :id');
        $stmt1->bindValue(':id', $id);
        $stmt1->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $idp = $stmt1->fetch();
    $idp = $idp['id_person'];
    try {
        $stmt = $this->db->prepare('DELETE FROM contact WHERE id_contact=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/detail-person?id=$idp");
})->setName('delete-contact');


$app->get('/add-contact', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $tplVars['id'] = $id;
    try {
        $stmt = $this->db->prepare('select name from contact_type');
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $tplVars['cont_types'] = $stmt->fetchAll();
    return $this->view->render($response, 'add-contact.latte', $tplVars);
})->setName('add-contact');

$app->post('/add-contact', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();  //$_POST
    try {
        $stmt = $this->db->prepare('select name,id_contact_type from contact_type');
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $contact_type = $stmt->fetchAll();
    $nic = true;
    foreach ($contact_type as $ct) {
        if (!empty($data[$ct['name']])) {
            try {
                $stmt = $this->db->prepare('INSERT INTO contact
                                          (id_person, id_contact_type, contact)
                                           VALUES
                                          (:id, :idt, :co)');
                $stmt->bindValue(':id', $id);
                $stmt->bindValue(':idt', $ct['id_contact_type']);
                $stmt->bindValue(':co', $data[$ct['name']]);
                $stmt->execute();
                $nic = false;
            } catch (Exception $ex) {
                if ($ex->getCode() == 23505) {
                    $tplVars['error'] = 'Tento kontakt uz existuje.';
                    $tplVars['form'] = $data;
                    $tplVars['cont_types'] = $contact_type;
                    return $this->view->render($response, 'add-contact.latte', $tplVars);
                } else {
                    $this->logger->error($ex->getMessage());
                    die($ex->getMessage());
                }
            }
        }
    }
    if ($nic) {
        $tplVars['error'] = 'Nie je zadaný žiaden kontakt.';
        $tplVars['form'] = $data;
        $tplVars['cont_types'] = $contact_type;
        return $this->view->render($response, 'add-contact.latte', $tplVars);
    }
    return $response->withHeader('Location', "/~xprchlik/Subory/devel/ErikPrchlik-slim-xprchlik-a7d6ca21cf87/public/auth/detail-person?id=$id");
});